<?php

class Mor extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function tampil_mor() {
        $hasil = $this->db->query('SELECT id, name FROM location_regionals');
        return $hasil;
    }

    public function simpan_mor($data) {
        $hasil = $this->db->insert('location_regionals', $data);
        return $hasil;
    }

    public function cari_mor($id) {
        $query = 'SELECT * FROM location_regionals  WHERE id=' . $id;
        $hasil = $this->db->query($query);
        return $hasil;
    }
    
    public function edit_mor($id_mor, $name){
        $this->db->set('name', $name);
        $this->db->set('updated_at', date('Y-m-d H:i:s'));
        $this->db->where('id', $id_mor);
        $hasil = $this->db->update('location_regionals');
    }
    
    public function delete($id_mor) {
        $hasil = $this->db->delete('location_regionals', array('id' => $id_mor));
        return $hasil;
    }

}
