

<div id="custom_form">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah MOR</h4>
    </div>
    <div class="modal-body">

        <div class="row">
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label">
                        Nama MOR
                    </label>
                </div>
                <div class="col-sm-9">

                    <input id="nama_mor" type="text" class="form-control" placeholder="MOR" value="">

                </div>

            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        <button onclick="simpan_mor()"type="button" class="btn btn-primary">Simpan</button>
    </div>
</div>
<script>

    function simpan_mor() {
        var nama_mor = $("#nama_mor").val();
        $.ajax({
            type: "get",
            url: "<?php echo base_url() ?>Mor_controller/simpan_mor/?nama=" + nama_mor,
            error: function (returnval) {
                alert("cek koneksi");
            },
            success: function (returnval) {

                $('#modal_tambah').modal("hide");
                $("#mor_table").DataTable().ajax.reload();
            }
        });
    }
</script>