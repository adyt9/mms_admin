<?php include ("core/header.php") ?>

<div class="content-wrapper" id="mor-content">


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Blank page
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Setting</a></li>
            <li class="active">MOR</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-6">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><a class="btn btn-success btn-small" onclick="tambah_mor()" href="#" ><span class="fa fa-plus"></span>
                                TAMBAH
                            </a>
                            <a id="refresh_dtt" href="#" class="btn btn-default btn-small"><span class="fa fa-refresh"></span></a>
                        </h3>

                    </div>

                    <div class="box-body">
                        <table id="mor_table" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">

                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal_tambah"  role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-content-tambah" style="height: auto; width:auto;">

            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_edit"  role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-content-edit" style="height: auto; width:auto;">

            </div>
        </div>
    </div>
</div>

<?php include 'core/footer.php' ?>
<script>
    $(document).ready(function () {
        var table = $('#mor_table').DataTable({
            "ajax": "<?php echo base_url() ?>Mor_controller/datatables",
        });
    });

    function tambah_mor() {
        $.ajax({
            type: "get",
            url: "<?php echo base_url() ?>Mor_controller/form_tambah_mor/",
            error: function (returnval) {
                alert("cek koneksi");
            },
            success: function (returnval) {
                $(".modal-content-tambah").html(returnval);
                $("#modal_tambah").modal("show");
            }
        });
        event.preventDefault();
    }

    function edit_mor(id_mor) {
        $.ajax({
            type: "get",
            url: "<?php echo base_url() ?>Mor_controller/form_edit_mor/" + id_mor,
            error: function (returnval) {
                alert("cek koneksi");
            },
            success: function (returnval) {
                $(".modal-content-tambah").html(returnval);
                $("#modal_tambah").modal("show");
            }
        });
        event.preventDefault();
    }

    function simpan_mor() {
        $.ajax({
            type: "get",
            url: "<?php echo base_url() ?>Load_file/load_html/tambah_mor/",
            error: function (returnval) {
                alert("cek koneksi");
            },
            success: function (returnval) {
                $(".modal-content-tambah").html(returnval);
                $("#modal_tambah").modal("show");
            }
        });
    }
    function hapus_mor(id_mor) {
        var r = confirm("Apakah yakin untuk menghapus ? ");
        if (r == true) {
            $.ajax({
                type: "get",
                url: "<?php echo base_url() ?>Mor_controller/delete_mor/" + id_mor,
                error: function (returnval) {
                    alert("cek koneksi");
                },
                success: function (returnval) {
                    $("#mor_table").DataTable().ajax.reload();
                }
            });
        }
    }
</script>
