<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Load_file extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Mor');
    }
    
    
    public function load_html($nama_fiew) {
        if ($this->session->userdata('username') == 'admin') {
            $this->load->view($nama_fiew);
        } else {
            $this->load->view('login');
        }
    }
    public function load_mor($nama_fiew) {
        if ($this->session->userdata('username') == 'admin') {
            $data['mor'] = $this->Mor->tampil_mor()->result_array();
            $this->load->view($nama_fiew, $data);
        } else {
            $this->load->view('login');
        }
    }
}
