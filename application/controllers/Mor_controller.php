<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mor_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Mor');
    }

    private function json_head() {
        header("Content-type:application/json");
    }

    public function datatables() {
        $dataMor = $this->Mor->tampil_mor()->result_array();

        $data['data'] = array();
        foreach ($dataMor as $var) {
            $format = array();
            foreach ($var as $val) {
                array_push($format, $val);
            }
            array_push($format, '
                <a href="#" onclick="edit_mor(' . $var['id'] . ');"><i class="fa fa-pencil-square-o"></i></a>
                                <a href="#" title="Hapus Data" onclick="hapus_mor(' . $var['id'] . ')"><i class="fa fa-trash-o"></i></a>
            ');
            array_push($data['data'], $format);
        }
        $this->json_head();
        echo json_encode($data);
    }

    public function mor() {
        $this->load->view('mor');
    }

    public function form_tambah_mor() {
        $this->load->view('mor/tambah_mor');
    }
    
    public function form_edit_mor($id_mor) {
        $hasil['dataMor'] = $this->Mor->cari_mor($id_mor)->result_array();
        $this->load->view('mor/edit_mor', $hasil);
    }

    public function simpan_mor() {
        $nama = $this->input->get('nama');
        $data = array(
            'name' => $nama,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );
        $hasil = $this->Mor->simpan_mor($data);
    }
    
    public function edit_mornya($id_mor) {
        $nama = $this->input->get('nama');
        $hasil = $this->Mor->edit_mor($id_mor, $nama);
    }
    
    public function delete_mor($id_mor) {
        $hasil = $this->Mor->delete($id_mor);
    }

}
