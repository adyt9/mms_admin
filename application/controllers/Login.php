<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {
        $this->load->view('login');
    }

    public function cek_login() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $hasil['user'] = $this->User->get_username($username, $password)->result_array();

        if ($hasil['user']) {
            foreach ($hasil['user'] as $value) {
                $this->session->set_userdata('username', $value['username']);
            }
            if ($this->session->userdata('username') == 'admin') {
                helper_log("add", "Login");
                $this->load->view('dashboard', $hasil);
            } else {
                $this->load->view('login');
            }
        } else {
            $data['notif'] = "Username atau Password salah";
            $this->load->view('login', $data);
        }
    }

    public function session_destroy() {
        $this->session->sess_destroy();
        helper_log("logut", "Logout");
        $this->load->view('login');
    }

}
